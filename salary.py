import os

import MySQLdb


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def get_operation_type(error=""):
    cls()
    if error:
        print error
    print("1. Brutton -> Netto")
    print("2. Netto -> Brutto")
    try:
        operation_type = int(raw_input("Podaj typ operacji: "))
    except ValueError:
        operation_type = get_operation_type("Podaj wartosc od 1 do 2.")
    if operation_type < 1 or operation_type > 2:
        operation_type = get_operation_type("Podaj wartosc od 1 do 2.")
    return operation_type


def get_vat(error=""):
    cls()
    if error:
        print error
    print("1. 3%")
    print("2. 5%")
    print("3. 7%")
    print("4. 8%")
    print("5. 22%")
    print("6. 23%")
    try:
        vat = int(raw_input("Podaj stawke VAT: "))
    except ValueError:
        vat = get_vat("Podaj wartosc od 1 do 6.")
    if vat < 1 or vat > 6:
        vat = get_vat("Podaj wartosc od 1 do 6.")
    cos = {
        1: 0.03,
        2: 0.05,
        3: 0.07,
        4: 0.08,
        5: 0.22,
        6: 0.23
    }
    return cos[vat]


def get_amount(error=""):
    cls()
    if error:
        print error
    try:
        amount = float(raw_input("Podaj kwote do przeliczen: "))
    except ValueError:
        amount = get_amount("Podaj poprawna kwote.")
    return amount


def calculate(operation_type, amount, vat):
    if operation_type == 1:
        tax = amount * vat / (vat + 1)
        return amount - tax
    if operation_type == 2:
        return amount * vat + amount


def save_in_db(operation_type, amount, vat, result):
    conn = MySQLdb.connect(host="mysql11.mydevil.net", user="m1359_salary", passwd="Salary12", db="m1359_salary")
    c = conn.cursor()
    c.execute("INSERT INTO salary (operation_type, amount, vat, result) VALUES (%s, %s, %s, %s)",
              (operation_type, amount, vat, result))
    conn.commit()
    c.close()


def main():
    operation_type = get_operation_type()
    vat = get_vat()
    amount = get_amount()
    result = calculate(operation_type, amount, vat)
    print("Wynik: " + str(result))
    save_in_db(operation_type, amount, vat, result)


if __name__ == '__main__':
    main()
